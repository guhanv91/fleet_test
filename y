log_level                :info
log_location             STDOUT
node_name                'root'
client_key               '/guhan/fleet_test/root.pem'
validation_client_name   'chef-validator'
validation_key           '/etc/chef-server/chef-validator.pem'
chef_server_url          'https://api.chef.io/organizations/fleetautomation'
syntax_check_cache_path  '/guhan/fleet_test/syntax_check_cache'
